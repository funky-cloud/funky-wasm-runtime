package garden.bots.funky

import io.vertx.ext.web.RoutingContext
import org.graalvm.polyglot.Context
import org.graalvm.polyglot.Source
import org.graalvm.polyglot.Value
import org.graalvm.polyglot.io.ByteSequence
import java.io.File

// WASM flavor
class Kompilo {

  private val contextBuilder: Context.Builder = Context.newBuilder("wasm")
  private val context: Context = contextBuilder.build()

  lateinit var funktion: Value

  fun loadWasm(wasm: String, functionName: String): Result<Value> {

    println("WasmCode")
    println(wasm)

    //java.util.Base64.getDecoder().decode(wasm)

    return try {
      //val binary: ByteArray = File(path).readBytes()
      //val charset = Charsets.UTF_8
      //val binary: ByteArray = wasm.toByteArray()
      val binary: ByteArray = java.util.Base64.getDecoder().decode(wasm)

      //val binary: ByteArray = wasm.toByteArray(charset)

      val sourceBuilder: Source.Builder = Source.newBuilder("wasm", ByteSequence.create(binary), "faas")
      val source = sourceBuilder.build()
      context.eval(source)
      val bindings = context.getBindings("wasm")
      funktion = bindings.getMember(functionName)
      println("===SUCCESS===")
      Result.success<Value>(funktion)
    } catch (exception: Exception) {
      println("===FAILURE===")
      println(exception.message)
      Result.failure<Value>(exception)
    }

  }


  fun invokeFunction(value: Int) : Result<Any> {
    return try {
      Result.success(funktion.execute(value))
    } catch (exception: Exception) {
      Result.failure<Any>(exception)
    }
  }



}
