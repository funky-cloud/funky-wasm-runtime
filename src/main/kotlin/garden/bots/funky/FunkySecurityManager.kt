package garden.bots.funky

import java.security.Permission

//open class SecurityManager
//http://www.java2s.com/Tutorial/Java/0490__Security/Defineyourownsecuritymanager.htm

class FunkySecurityManager() : SecurityManager() {

  override fun checkExit(status: Int) {
    super.checkExit(status)
    throw Exception("😡 exit is ⛔️ [${this.securityContext.toString()}]")
    //throw java.lang.SecurityException("😡 exit is ⛔️ [${this.securityContext.toString()}]")
  }


  override fun checkRead(fileName: String?) {
    //super.checkRead(fileName)
    //throw SecurityException(" You are not allowed to read files")
  }



  override fun checkWrite(fileName: String?) {
    super.checkWrite(fileName)
    throw SecurityException(" You are not allowed to write files")
  }

  override fun checkDelete(fileName: String?) {
    super.checkDelete(fileName)
    throw SecurityException(" You are not allowed to delete files")
  }

  override fun checkPermission(perm: Permission?, context: Any?) {
    //super.checkPermission(perm, context)
    //println("permission + cxt: $perm.toString()")
  }

  override fun checkPermission(perm: Permission?) {
    //super.checkPermission(perm)
    //println("permission: $perm.toString()")
  }

  override fun checkPackageAccess(pkg: String?) {
    /*
    super.checkPackageAccess(pkg)
    if (pkg != null) {
      if(pkg.startsWith("garden.bots.funky")) {
        throw Exception("😡 a funktion is using $pkg")
      }
    }
    */
  }

  override fun checkPackageDefinition(pkg: String?) {
    //super.checkPackageDefinition(pkg)
    //println("package definition: $pkg")
  }

  override fun checkAccess(t: Thread?) {
    //super.checkAccess(t)
    //println("> checkAccess t: ${t.toString()}")
  }


}
