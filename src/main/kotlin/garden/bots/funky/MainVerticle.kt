package garden.bots.funky

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.ext.healthchecks.HealthCheckHandler
import io.vertx.ext.healthchecks.Status
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.servicediscovery.Record
import io.vertx.servicediscovery.ServiceDiscovery
import io.vertx.servicediscovery.ServiceDiscoveryOptions
import io.vertx.servicediscovery.rest.ServiceDiscoveryRestEndpoint
import io.vertx.servicediscovery.types.HttpEndpoint
import java.util.*


class MainVerticle : AbstractVerticle() {
  //private lateinit var discovery: ServiceDiscovery
  //private lateinit var record: Record

  override fun stop(stopPromise: Promise<Void>) {
    println("👋 bye bye")
    stopPromise.complete()
  }

  override fun start(startPromise: Promise<Void>) {
    val startedAt = Date()
    lateinit var lastCall: Date
    println("🤖 started at: $startedAt")

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    // Health check handler
    val healthCheck = HealthCheckHandler.create(vertx)

    val kompilo = Kompilo()
    val language = System.getenv("LANG") ?: "js"
    val httpPort = System.getenv("PORT")?.toInt() ?: 8080
    val contentType = System.getenv("CONTENT_TYPE") ?: "application/json;charset=UTF-8"
    val functionName = System.getenv("FUNCTION_NAME") ?: "main"

    val functionCode = System.getenv("FUNCTION_CODE") ?: """
      function ${functionName}(params) {
        return {
          message: "👋 Hello World 🌍",
        }
      }
    """.trimIndent()

    //val compiledFunction = kompilo.compileFunction(functionCode, language)
    val compiledFunction = kompilo.loadWasm(functionCode, functionName)

    compiledFunction.let {
      when {
        it.isFailure -> { // compilation error
          healthCheck.register("compilation") { promise -> promise.complete(Status.KO()) }

          router.post("/").handler { context ->
            context.response().putHeader("content-type", "application/json;charset=UTF-8")
              .end(json { obj("compilationError" to it.exceptionOrNull()?.message) }.encodePrettily())
          }
        }

        it.isSuccess -> { // compilation is OK, and name of the function to invoke is "handle"
          healthCheck.register("compilation") { promise -> promise.complete(Status.OK()) }

          router.post("/").handler { context ->
            val params = context.bodyAsJson
            // call the function

            kompilo.invokeFunction(params.getInteger("value")).let {
              //context.request().headers()
              lastCall = Date()

              when {
                it.isFailure -> { // execution error
                  context.response().putHeader("content-type", "application/json;charset=UTF-8")
                    .end(json { obj("ExecutionError" to it.exceptionOrNull()?.message) }.encodePrettily())
                }
                it.isSuccess -> { // execution is OK
                  val result = it.getOrNull()
                  context.response().putHeader("content-type", contentType)
                    .end(result.toString())
                }
                else -> { TODO() }
              }
            }
          }
        }
        else -> { TODO() }
      }
    }

    router.get("/time").handler { context ->
      context.response().putHeader("content-type", "application/json;charset=UTF-8")
        .end(
          json {
            obj(
              "startedAt" to startedAt.toString(),
              "lastCall" to lastCall.toString(),
              "sinceLastCall" to Date().time - lastCall.time
            )
          }.encodePrettily()
        )
    }

    router.get("/").handler { context ->
      // call the index function
      lastCall = Date()
      context.response()
        .putHeader("content-type", "text/html;charset=UTF-8")
        .end("<h1>WASM</h1>".toString())
    }

    // link/bind healthCheck to a route
    router.get("/health").handler(healthCheck)

    //System.setSecurityManager(FunkySecurityManager())

    vertx
      .createHttpServer()
      .requestHandler(router)
      .listen(httpPort) { http ->
        when {
          http.failed() -> {
            startPromise.fail(http.cause())
          }
          http.succeeded() -> {
            println("🤖 GraalVM funky runtime for $functionName funktion started on port $httpPort")

            startPromise.complete()
          }
        }
      }
  }
}
