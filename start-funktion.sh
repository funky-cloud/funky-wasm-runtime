#!/bin/bash

PORT=9999
PROJECT="hello_rust"

function deploy() {
  PORT=$1 LANG="wasm" \
  FUNCTION_CODE="$(cat ./samples/hello_rust/hello_rust.gc.wasm | base64)" \
  java -jar ./target/funky-wasm-runtime-1.0.0-SNAPSHOT-fat.jar & echo $!
}

function destroy() {
  echo "🖐️ function with id:$FUNCTION_ID will be destroyed in $1 secs."
  sleep $1
  kill -9 $2
}

{
  deploy $PORT
} &> /dev/null

# TODO call the health check etc ... to check if the function is ok
# And display something
FUNCTION_ID=$!
echo "🚀 function with id:$FUNCTION_ID is started and is listening on http port: $PORT"

destroy 600 $FUNCTION_ID &

